<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 
<style type="text/css">
	.banner {
		background-image: url('public/img/banner/photo-1519120693210-d47b03b31d77.jpg');
	}
	.header .search {
		text-align: center;
	}
	.header .clearfix {
		border-color: black;
		background-color: black;
	}
	.header-top {
		background-color: whitesmoke;
    	padding: 5px 0px;
    	font-weight: bold;
	}
	.header-top a{
		color: #ED2F2F;
		text-decoration: none;
    	
	}
	.list-brands {
		background-color: #2B281E;
		color: white;
    	padding-top: 10px;
	}
	.list-brands a {
		color: whitesmoke;
		font-weight: 500;
	}
	#menu_top {
		padding: 0px 0px;
		text-align: bottom;
	}
	#menu_top p {
		font-size: 15px;
		text-align: center;
		color: black;
	}
	.item-menu {
		margin: 0px;
		margin-top: auto;
	}
	/*.item-menu :hover {
		background-color: #F2F2F2;
	}*/
	.item-menu a {
		margin-top: 7px;
	}
	#timkiem {
		width: 170px;
    	float: left;
	}
	#search22{
		margin-top: 10px;
	}
	.item-menu i {
		color: black;
	}
	.clearfix .row a{
		text-decoration: none;
	}
</style>
<div id="support-top" style="display: block;border-bottom: 1px solid #ddd; ">
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-6">
							<div class="address-admin">
								<a href="?c=home&m=index"><i class="fa fa-home"></i> 337 Cầu giấy, Hà nội</a>
							</div>					
						</div>
						<div class="col-md-6">
							<div class="phone-admin">
								<a href=""><i class="fa fa-phone"></i> 0914249694</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6" style="display: block;text-align: right;">
					<?php  
						if (isset($_SESSION['customer'])) :
					?>
					<a href="?c=customer&m=profile&email=<?php echo $_SESSION['customer']['email']; ?>"><i class="fa fa-user"></i> &nbsp;<?php echo $_SESSION['customer']['fullname']; ?></a>
					&nbsp;
					<a href="?c=customer&m=logout"> Đăng xuất</a>
					&nbsp;
					<?php  
						else:
					?>
					<a href="?c=customer&m=register">Đăng kí</a>
					&nbsp;
					<a href="?c=customer&m=login">Đăng nhập</a>
					<?php  
						endif;
					?>
				</div>
			</div>
			<div class="clearfix"></div>
		</div> <!-- .container -->
	</div> <!-- .header-top -->
</div> <!-- #header -->
<div id="header" style="background: #fed700;">
	<div class="container">
		<div class="row">
			<div class="col-md-4 clearfix" id="search_left1">
				<div class="row" style="padding-top: 5px;">
					<div class="col-md-3">
						<div class="row">
							<a href="?c=home&m=index"><img src="public/img/logo1.png" style="width: 100%;"></a>
						</div>
					</div>
					<div class="col-md-9">
						<div class="row">
							<div id="search22">
								<form action="?c=search&m=index" method="POST">
		    						<input type="text" class="form-control" placeholder="Search" name="str" id="timkiem">
		    					 <button class="btn btn-primary" type="submit" name="submit"><i class="glyphicon glyphicon-search"></i></button>  
		    						 
		    					</form>	 
							</div>
						</div>
						
					</div>
					
				</div>
			</div>

			<div class="col-md-7" id="menu_top">
				<div class="row">
					<div class="col-md-2 item-menu">
						<a href="?c=home&m=index"><i class="fas fa-home fa-2x"></i><p>Trang chủ</p></a>
					</div>
					<div class="col-md-2 item-menu">
						<a href=""><i class="fas fa-headphones fa-2x"></i>
						<p>Phụ kiện</p></a>
					</div>
					<div class="col-md-2 item-menu">
						<a href=""><i class="fas fa-mobile-alt fa-2x"></i>
						<p>Máy cũ <!-- giá rẻ --></p></a>
					</div>
					<div class="col-md-2 item-menu">
						<a href=""><i class="fas fa-wrench fa-2x"></i>
						<p>Sửa chữa</p></a>
					</div>
					<div class="col-md-2 item-menu">
						<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 110.83px; padding-top: 0px;">
					          <p id="your_cart" style=" margin: 0px 0 0px;"><i class="fas fa-cart-plus fa-2x"></i> <?php echo $totalQTY;?></p>
					        </a>

					        <?php  
					        if($totalQTY > 0):
					        ?>
					        
					         <div class="dropdown-menu" aria-labelledby="navbarDropdown">
					         	<div id="your_cart_items">
					        	<?php  
					        		if (isset($_SESSION['cart']) && count($_SESSION['cart'])) :
					        			$total = 0 ;
					        			foreach ($_SESSION['cart'] as $item) :
					        	?>
					          <a class="dropdown-item" href="#"><?php echo $item['name']; ?></a>
					          	
					          <?php  
					          				$total += ($item['price'] * $item['qty']);
					          			endforeach;
					          ?>
					      		</div>
					          <div class="dropdown-divider"></div>
					          <a class="dropdown-item" id="cart_total" href="?c=cart&m=index">Tổng : <?php echo number_format($total); ?> VND</a>
					          
					          <?php  
					          	else :
					          ?>
					          <a href="">No item</a>
					          <?php  
					          	endif;
					          ?>
					        </div>
					        <?php 
					        endif;
					        ?>
					</div>
					<div class="col-md-2 item-menu">
						<a href=""><i class="fas fa-fax fa-2x" ></i>
							<p>Liên hệ</p></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="banner">
	<div class="container">
		<div class="row">
			<div class="col-md-9 banner" style="padding-left: 25px;">
				
				<img  name="image" src="public/img/banner/example-slide-1.jpg" alt="" style="width: 100%">
			
				
			</div>
			
			<div class="col-md-3">
				<img src="public/img/banner/bnleft-min.png" style="width: 390px;">
			</div>
		</div>
	</div>
</div>
<div class="list-brands clearfix">
	<div class="container">
		<div class="row">
			<p>Hãng</p>
			<a href="?c=brand&m=view&id=1">Apple</a>
			<a href="?c=brand&m=view&id=4">Xiaomi</a>
			<a href="?c=brand&m=view&id=2">Sony</a>
			<a href="?c=brand&m=view&id=3">Asus</a>
			<a href="?c=brand&m=view&id=5">Samsung</a>
			<a href="">Phụ kiện</a>

		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.banner .owl-carousel').owlCarousel({
	    loop:true,
	    margin:10,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        }
	    }
	});
	})
</script>

<script type="text/javascript">
	$(document).ready(function(){
		//$('.owl-dots').css('display','none');
		$('#submitSearch').click(function(){
			var str = $('#searchName').val();
			alert(str);
			$.ajax({

				url:"index.php?c=search&m=index",

				method:"POST",

				dataType:"json",

				data:{str:str},

				success: function(res){
					alert("vao den day");
					console.log(res);
				}
			});
		});
	})
</script>

<script type="text/javascript">
	//alert(1);
  window.onload = function(){
    setTimeout("switch_Image()", 3000);
  }
  var current = 1;
  var num_image = 4;
  function switch_Image(){
      current++;
      document.images['image'].src ='public/img/banner/example-slide-' + current + '.jpg';
     if(current < num_image){
       setTimeout("switch_Image()", 3000);
     }else if(current == num_image){
       current = 0;
       setTimeout("switch_Image()", 3000);
     }
 }
</script>
<!-- <script type="text/javascript">
	$(document).ready(function){
		alert(1);
		// $('#an_tim_kiem').click(function(){
		// 	alert(1);
		// 	var str = $("#timkiem").val();
		// 	$.ajax({

		// 		url:"index.php?c=search&m=index",

		// 		method:"GET",

		// 		dataType:"json",

		// 		data:{data:str},
		// 		success:function(res){
		// 			console.log(res);
		// 		}

		// 	});
		// });
		
	}

	$(document).ready(function(){
		alert(1);
	})
</script> -->