<div class="container" style="margin-top: 20px;">
	
	<form action="?c=order&m=addOrder" method="POST" id="form-buy">
		<h2>Thông tin đơn hàng</h2>
			<div class="message">
			<!-- Thông báo lỗi  -->
			<?php 
				if (count($errors) > 0) :
					for ($i = 0; $i < count($errors); $i++) :
			?>
				<p class="errors" style="color: red;"><?php echo $errors[$i];?></p>
				<?php 
					endfor;
				endif ;
			?><!-- end errors -->
		</div>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Họ tên</label>
		    <input type="text" class="form-control" id="hoten" name="hoten" value="<?php if (isset($_POST['hoten'])) echo $_POST['hoten'] ;?>">	    
		  </div>
		   <div class="form-group">
		    <label for="exampleInputEmail1">Email </label>
		    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" name="email" value="<?php if (isset($_POST['email'])) echo $_POST['email'] ;?>">
		  </div>
		   <div class="form-group">
		    <label for="exampleInputEmail1">Số điện thoại</label>
		    <input type="text" class="form-control" id="phone" name="phone" value="<?php if (isset($_POST['phone'])) echo $_POST['phone'] ;?>">	    
		  </div>
		   <div class="form-group">
		    <label for="exampleInputEmail1">Địa chỉ</label>
		    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="address" value="<?php if (isset($_POST['address'])) echo $_POST['address'] ;?>"></textarea>	     
		  </div>

			<div class="form-group">
		      <tr>
			<td>Tỉnh/Thành phố:</td>

			<td>
				<select name="province_id" id="optionProvince">
					<option value="">Chọn</option>
					<?php  
					if (count($provinces) > 0):
						foreach ($provinces as $province):
					?>
					<option value="<?php echo $province['id'] ;?>" 
						<?php if ((isset($_POST['province_id'])) && $_POST['province_id'] == $province['id']) echo 'selected = "selected" ' ; ?>	
					>
						<?php echo $province['name'] ;?>
							
					</option>
					<?php  
						endforeach ;
					endif;
					?>
				</select>
			</td>
		</tr> 
		  </div>
		  
		<br>
		<tr>
			<td>Huyện/Quận:</td>
			<td>
				<select name="district_id" id="optionDistrict">
					<option value="">Chọn</option>
					
				</select>
			</td>
		</tr>

		  <div class="form-group row">
		    <label for="staticEmail" class="col-sm-2 col-form-label">Tổng tiền</label>
		    <div class="col-sm-10">
		      <input type="text" readonly class="form-control-plaintext" id="amount" value="<?php echo number_format($total) .'&nbsp;' .'VND'; ?>" name="amount">
		    </div>
		  </div>
		  <div class="form-check">
		    <input type="checkbox" class="form-check-input" id="exampleCheck1">
		    <label style="padding-left: 20px;" class="form-check-label" for="exampleCheck1" > Thanh toán trực tiếp</label>
		  </div>
		  <div class="form-group" style="display: none;">
		    <label for="exampleInputEmail1">Hidden </label>
		    <input type="text" class="form-control" id="infoUser" value="<?php if(isset($_SESSION['customer'])) echo $_SESSION['customer']['id']; ?>" name="userId" readonly="">
		  </div>
		  <button type="submit" class="btn btn-primary" name="submit">Thanh toán</button>
	</form>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#optionProvince').change(function(){
			var provinceId = $(this).val();
			//alert(provinceId);
			$.ajax({
				url: '?c=customer&m=district&province_id=' + provinceId,
				method: 'GET',
				success: function(res)
				{
					$('#optionDistrict').empty().html(res);
				}
			})
		});
	});
</script>