
<style type="text/css">
	#home-brand {
		display: none;
	}
	/*#btn {
		padding: 8px 30px 8px 13px;
	}ti

</style>


<div class="content">
	<div class="container">
		<div class="products">
			<div class="products-header">
				<div class="row">
					<div class="col-md-12 box-content-left-fix header">
						<a href="">
							Sản phẩm mới nhất
						</a>
						<hr>
					</div>
				</div>
			</div>
			<div class="main-products" >
				<div class="row">
					<?php  
						foreach ($productsNew as $item) :
					?>
					<div class="col-md-3 item-product">
						<div class="thumbnail">
							<div class="image-product">
								<a href="?c=product&m=view&id=<?php echo $item['id']; ?>">
									<img src="admin/<?php echo $item['img']; ?>">
								</a>
							</div>
							<div class="caption text-center">
								<div class="caption-name">
									<h2>
										<a href="?c=product&m=view&id=<?php echo $item['id']; ?>" style="font-size: 14px;" >
											<?php  
												echo $item['name'];
											?> 
										</a>
									</h2>
									<b>
										<strong>
											<span class="text-danger">
												Giá: <?php echo number_format($item['price']); ?>	
											</span>
										</strong>
									</b>
								</div>
								<!-- <div class="caption-cart btn-group  btn-group-justified" role="group"> -->
								<div class="caption-cart btn-group " role="group">
									<a href="?c=product&m=view&id=<?php echo $item['id']; ?>" class="btn btn-primary">
										<i class="far fa-eye"></i>
										Chi Tiết
									</a> 

									<button type="button" class="btn btn-success buyNow" id="btn" role="button" product="<?php echo $item['id']; ?>">
										<i class="fas fa-cart-plus"></i>

										Thêm vào giỏ
									</button>
								</div>
							</div>
						</div>

					</div>
					<?php  
						endforeach;
					?>
					
				</div>
			</div>
		</div>

		<div class="products">
			<div class="products-header">
				<div class="row">
					<div class="col-md-12 box-content-left-fix header">
						<a href="">
							Sản phẩm khuyến mại
						</a>
						<hr>
					</div>
				</div>
			</div>
			<div class="main-products">
				<div class="row">
					<?php  
						foreach ($productsSale as $item) :
					?>
					<div class="col-md-3 item-product">
						<div class="thumbnail">
							<div class="image-product">
								<a href="?c=product&m=view&id=<?php echo $item['id']; ?>">
									<img src="admin/<?php echo $item['img']; ?>" >
								</a>
							</div>
							<div class="caption text-center">
								<div class="caption-name">
									<h2>
										<a href="?c=product&m=view&id=<?php echo $item['id']; ?>" style="font-size: 14px;"><?php echo $item['name']; ?></a>
									</h2>
									<b>
										<strong>
											<span class="text-danger">
												Giá: <?php echo number_format($item['price']); ?>	
											</span>
										</strong>
									</b>
								</div>
								<!-- <div class="caption-cart btn-group  btn-group-justified" role="group"> -->
								<div class="caption-cart btn-group " role="group">
									<a href="?c=product&m=view&id=<?php echo $item['id']; ?>" class="btn btn-primary">
										<i class="far fa-eye"></i>
										Chi Tiết
									</a> 

									<button href="#" class="btn btn-success buyNow" id="btn" role="button" product="<?php echo $item['id']; ?>">
										<i class="fas fa-cart-plus"></i>
										Thêm vào giỏ 
									</button>
								</div>
							</div>
						</div>

					</div>
					<?php  
						endforeach;
					?>
					
				</div>
			</div>
		</div>
		<div class="products">
			<div class="products-header">
				<div class="row">
					<div class="col-md-12 box-content-left-fix header">
						<a href="">
							Sản phẩm HOT 
						</a>
						<hr>
					</div>
				</div>
			</div>
			<div class="main-products">
				<div class="row">
					<?php  
						foreach ($productsHot as $item) :
					?>
					<div class="col-md-3 item-product">
						<div class="thumbnail">
							<div class="image-product">
								<a href="?c=product&m=view&id=<?php echo $item['id']; ?>">
									<img src="admin/<?php echo $item['img']; ?>" style="height: 255px;" >
								</a>
							</div>
							<div class="caption text-center">
								<div class="caption-name">
									<h2>
										<a href="?c=product&m=view&id=<?php echo $item['id']; ?>" style="font-size: 14px;"><?php echo $item['name']; ?></a>
									</h2>
									<b>
										<strong>
											<span class="text-danger">
												Giá: <?php echo number_format($item['price']); ?>	
											</span>
										</strong>
									</b>
								</div>
								<!-- <div class="caption-cart btn-group  btn-group-justified" role="group"> -->
								<div class="caption-cart btn-group " role="group">
									<a href="?c=product&m=view&id=<?php echo $item['id']; ?>" class="btn btn-primary">
										<i class="far fa-eye"></i>
										Chi Tiết
									</a> 

									<button type="button" class="btn btn-success buyNow" id="btn" role="button" product="<?php echo $item['id']; ?>">
										<i class="fas fa-cart-plus"></i>


									Thêm vào giỏ 
								</div>
							</div>
						</div>

					</div>
					<?php  
						endforeach;
					?>
					
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.buyNow').click(function(){

			let idProduct = $(this).attr("product");
			console.log(idProduct);
			$.ajax({
				url:"index.php?c=cart&m=addCart",

				method:"GET",

				dataType:"json",

				data:{id:idProduct},

				success:function(res){
					//alert("Đã thêm vào giỏ hàng");
					console.log(res);
					$('#your_cart').empty().html('(' + res.totalQTY + ')');
					$('#your_cart_items').empty().append(res.itemHTML);
					$('#cart_total').empty().append(res.subtotal);
					return false;
				}
			});

		})
	});
</script>