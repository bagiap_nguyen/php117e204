<?php  
include('menu_left.php');
?>

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Sản phẩm mua hàng của mã hàng #<?php echo $price['order_id']; ?></h1>
 </div>

<table style="" border="1">
	<thead>
        <tr>
            <th>STT</th>
            <th style="width: 50px;">Mã đơn hàng</th>
            <th>Khách hàng</th>
            <th>Email</th>
            <th>Sản phẩm</th>
            <th>Hình ảnh</th>
            <th>Giá sản phẩm</th>

            
        </tr>
    </thead>

    <tbody>
        <?php
        if (count($orders) > 0) : 
            $i = 0;
            foreach ($orders as $item) :
                $i++;
                ?>
                <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $item['order_id'];?></td>
                    <td><?php echo $item['fullname'];?></td>
                    <td><?php echo $item['email'];?></td>
                    <td><?php echo $item['products'];?></td>
                    <td><img style="width: 100px;" src="<?php echo $item['img'] ;?>"></td>
                    <td><?php echo number_format($item['price']) . " VND" ;?></td>
                </tr>
                <?php
            endforeach;
            ?>
             <tr>
             	<td colspan="5" style="border-left: 1px solid #FffF; border-bottom: 1px solid #FffF;"></td>
            	<td>Tổng tiền:</td>
            	<td><?php if (count($price) > 0) echo number_format($price['total_price']) . "VND" ; else echo '0 VND';?> </td>
            </tr>
            <?php
        else: 
            ?>
            <tr><td colspan="7">Chưa có bản ghi</td></tr>
            <?php
        endif; 
        ?>

    </tbody>
</table>

<?php  
include('footer.php');
?>