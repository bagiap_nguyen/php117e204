<style type="text/css">
	img {
		width: 100%;
	}
	.widget-title {
		height: 40px;
		line-height: 40px;
	    font-size: 16px;
	    text-transform: uppercase;
	    border: 1px solid #e1e1e1;
	    padding-left: 20px;
	    border-left: 3px solid #606366;
	    color: #606366;
	}
</style>
<br>
<div class="container">
	<div class="row">
		<div class="col-md-9">
			<div class="row">
				<div class="col-md-5">
					<img src="admin/<?php echo $post['img']; ?>">
				</div>
				<div class="col-md-7">
					<h1><?php echo $post['name'] ;?></h1>
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-10">
					<h4><?php echo $post['description']; ?></h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10">
					<?php echo $post['content']; ?>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<!-- Phụ kiện -->
			<div class="widget">
				<h3 class="widget-title">Phụ kiện</h3>
				<?php  
					foreach ($extras as $item) :
				?>

				<div class="row">
					<div class="col-sm-6">
						<a class="pull-left" href="?c=product&m=view&id=<?php echo $item['id']; ?>"><img style="width: 100%" src="admin/<?php echo $item['img']; ?>" alt=""></a>
					</div>
					<div class="col-sm-6">
						<a href="?c=product&m=view&id=<?php echo $item['id']; ?>"><?php echo $item['name']; ?></a><br>
						<p>
							<?php echo number_format($item['price']) . "₫"; ?>
						</p>
					</div>

				</div><br>

				<?php  
					endforeach;
				?>
			</div> <!-- best sellers widget phụ kiện-->
		</div>
	</div>
</div>

