<style type="text/css">
	.new-main a {
		text-decoration: none;
		display: block;
		text-align: left;
	}
	#post_all {
		float: right;
		font-size: 20px;
		text-transform: none;
		color: #7F6609;
	}
</style>
<div class="news">
	<div class="new-heading">
		<div class="container">
			<div class="row">
				<div class="col-md-12 heading-new">
					<a href="">
						Tin Tức Mới
					</a>
					<a href="?c=post&m=list" id="post_all">Tất cả</a>
					
				</div>
			</div>
		</div>
	</div>

	<div class="new-main">
		<div class="container">
			<div class="row">
				<?php  
					foreach ($posts as $item) :
				?>

				<div class="col-md-4 new-item">
					<div class="new-img">
						<a href="?c=post&m=view&id=<?php echo $item['id']; ?>">
							<img style="width: 270px; height: 135px;" src="admin/<?php echo $item['img']; ?>">
						</a>
					</div>
					<div class="new-title">
						<a href="?c=post&m=view&id=<?php echo $item['id']; ?>" style="padding-left: 30px; font-size: 13px;">
							<?php echo $item['name'] ;?>
						</a>
					</div>
				</div>

				<?php  
					endforeach;
				?>
			</div>
		</div>
	</div>
</div>
