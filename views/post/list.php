<style type="text/css">
	.aside-menu {
	    list-style: none;
	    background: #f8f8f8;
	    border: 1px solid #e5e4e4;
	    padding: 25px;
	}
	.aside-menu li {
    	padding: 10px 0;
    	border-bottom: 1px solid #e5e4e4;
	}

	.page-item {
		margin-left: 10px !important;
	}
	.widget-title {
		height: 40px;
		line-height: 40px;
	    font-size: 16px;
	    text-transform: uppercase;
	    border: 1px solid #e1e1e1;
	    padding-left: 20px;
	    border-left: 3px solid #606366;
	    color: #606366;
	}
	.beta-lists>* {
	    padding: 20px;
	    border-bottom: 1px dotted #e1e1e1; 
	}


</style>

<div class="container">
	<div id="content" class="space-top-none">
		<div class="main-content">
			<div class="space60">&nbsp;</div>
			<div class="row">
				<!-- Danh sách tin tức -->
				<div class="col-sm-9 col-md-9">
					<div class="beta-posts-list">
						
						<?php  
							foreach ($listPost as $item) :
						?>

						<div class="row">
							<div class="col-sm-4">
								<a href="?c=post&m=view&id=<?php echo $item['id']; ?>"><img src="admin/<?php echo $item['img']; ?>" width="270px"></a>
							</div>
							<div class="col-sm-5">
								<div class="row">
									<a href="?c=post&m=view&id=<?php echo $item['id']; ?>">
										<?php echo $item['name'] ;?>
									</a>
								</div>
								<div class="row">
									<p class="noidung">
										<?php echo substr($item['content'], 0, 250) . "..." ;?>
										<a href="?c=post&m=view&id=<?php echo $item['id']; ?>" style="margin-left: 0px;margin-right: 0px;">Đọc tiếp</a>...
									</p>
								</div>
							</div>
						</div>
						<br>

						<?php  
							endforeach;
						?>
					
					</div> <!-- .beta-posts-list -->

					<div class="space50">&nbsp;</div>
					<!-- Phân trang -->
					<div class="page" style="text-align: center;">
						<nav aria-label="Page navigation example">
						  <ul class="pagination justify-content-center" >
						   <!--  <li class="page-item disabled"> -->
						   	<li>
						    	<?php
						      		if ($currentPage > 1 && $totalPages > 1):
						      	?>
	   								<a href="?c=post&m=list&pages=<?php echo ($currentPage-1) ;?>">Prev</a>
								<?php
									endif;
								?>
						    </li>
						    <?php  
						    	for ($i = 1; $i <= $totalPages; $i++) :

						    ?>
						    <!-- <li class="page-item"> -->
						    <li>
						    	<a class="page-link" href="?c=post&m=list&pages=<?php echo $i; ?>"><?php echo $i; ?></a>
						    </li>

						    <?php  
						    	endfor;
						    ?>

						    <!-- <li class="page-item"> -->
						    <li>
						    	<?php
						      		if ($currentPage < $totalPages && $totalPages > 1):
						      	?>
	   								<a href="?c=post&m=list&pages=<?php echo ($currentPage+1) ;?>">Next</a>
								<?php
									endif;
								?>
						    </li>
						  </ul>
						</nav>
					</div><!-- Kết thúc phân trang -->

				</div><!-- kết thúc danh sách tin tức -->

				<div class="col-sm-3 aside">
					<!-- Phụ kiện -->
					<div class="widget">
						<h3 class="widget-title">Phụ kiện</h3>
						<?php  
							foreach ($extras as $item) :
						?>

						<div class="row">
							<div class="col-sm-6">
								<a class="pull-left" href="?c=product&m=view&id=<?php echo $item['id']; ?>"><img style="width: 100%" src="admin/<?php echo $item['img']; ?>" alt=""></a>
							</div>
							<div class="col-sm-6">
								<a href="?c=product&m=view&id=<?php echo $item['id']; ?>"><?php echo $item['name']; ?></a><br>
								<p>
									<?php echo number_format($item['price']) . "₫"; ?>
								</p>
							</div>

						</div><br>

						<?php  
							endforeach;
						?>
					</div> <!-- best sellers widget phụ kiện-->
				</div>

			</div> <!-- end section with sidebar and main content -->
		</div> <!-- .main-content -->
	</div> <!-- #content -->
	
</div> <!-- .container -->