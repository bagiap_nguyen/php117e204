<?php  

class Post extends Database {

	public function getPosts($field = '*',$join = '', $where = '', $orderby = '')
	{
		$condition = '';
		if ($where != '') {
			$condition = 'AND ' . $where;
		}

		$sql = sprintf("SELECT %s FROM posts %s WHERE 1=1 %s %s", $field, $join, $condition, $orderby);
		//echo $sql; exit();
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_all(MYSQLI_ASSOC);
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

	public function getPost($field, $join, $where)
	{
		$condition = '';
		if ($where != '') {
			$condition = 'AND ' . $where;
		}

		$sql = sprintf("SELECT %s FROM posts %s WHERE 1=1 %s", $field, $join, $condition);
		//echo $sql; exit();
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_assoc();
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

	public function totalRecordPost()
	{
		$sql = sprintf("SELECT COUNT(*) as totalRecord FROM posts");

		try {

			$query = $this->_connect->query($sql);

			return $query->fetch_assoc();

		} catch (Exception $e) {
			$e->getMessage();
		}

		return null;
	}

	public function getListPost($field = '*',$join = '',$where = '' ,$orderBy = "null", $limit = '', $offset = '')
	{
		$condition = '';

		if ($where != '') {
			$condition = 'AND' . $where;
		}

		$sql = sprintf("SELECT %s FROM posts  %s WHERE 1=1 %s %s %s %s",$field,$join,$condition , $orderBy,$limit,$offset);
		//echo $sql; exit();
		try {
			$query = $this->_connect->query($sql);

			return $query->fetch_all(MYSQLI_ASSOC);

		} catch (Exception $e) {
			$e->getMessage();
		}

		return null;
	}

	public function getProductExtras($field, $join, $where, $limit, $offset, $orderBy)
	{
		$condition = '';
		if ($where != '') {
			$condition = 'AND ' . $where;
		}
		$sql = sprintf("SELECT %s FROM products %s WHERE 1=1 %s ORDER BY %s LIMIT %s OFFSET %s", $field, $join, $condition, $orderBy, $limit, $offset);
		//echo $sql; exit();
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_all(MYSQLI_ASSOC);
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

}

?>