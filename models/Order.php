<?php  
	/**
	 * Order
	 */
	class Order extends Database
	{
		public function addOrder($userId,$fullname,$email,$amount,$phone,$address, $province_id, $district_id,$note,$carts)
		{
			$created_at = date('Y-m-d');
			$updated_at = date('Y-m-d');

			//var_dump($_SESSION['cart']); exit();
			$sql = sprintf("INSERT INTO orders
				(user_id,
				fullname,
				email,
				phone,
				address,
				province_id,
				district_id,
				amount,
				note,
				created_at,
				updated_at
				) 
			VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
						"'{$userId}'",
						"'{$fullname}'",
						"'{$email}'",						
						"'{$phone}'",
						"'{$address}'",
						"'{$province_id}'",
						"'{$district_id}'",
						"'{$amount}'",
						"'{$note}'",
						"'{$created_at}'",
						"'{$updated_at}'"
					);
			//echo $sql; exit();
			try {
				$query = $this->_connect->query($sql);
				if($query) {
					return $this->_connect->insert_id;
				}

			} catch (Exception $e){
				$e->getMessage();
			}
			return "them that bai";
		}

		public function addOrderNew($fullname,$email,$amount,$phone,$address, $province_id, $district_id,$note,$carts)
		{
			$created_at = date('Y-m-d');
			$updated_at = date('Y-m-d');

			$sql = sprintf("INSERT INTO orders
				(fullname,
				email,
				phone,
				address,
				province_id,
				district_id,
				amount,
				note,
				created_at,
				updated_at
				) 
			VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
						"'{$fullname}'",
						"'{$email}'",						
						"'{$phone}'",
						"'{$address}'",
						"'{$province_id}'",
						"'{$district_id}'",
						"'{$amount}'",
						"'{$note}'",
						"'{$created_at}'",
						"'{$updated_at}'"
					);
			//echo $sql; exit();
			try {
				$query = $this->_connect->query($sql);
				if($query){
					return $this->_connect->insert_id;
				
				}
			} catch (Exception $e){
				$e->getMessage();
			}
			return "them that bai";
		}

		public function addOrderItems($order_id, $carts)
		{
				//echo $order_id;
				foreach ($carts as $cart) {
				//var_dump($cart);
					$productId = $cart['id'];
					$price = $cart['price'];
					$qty = $cart['qty'];

					$sql = sprintf("INSERT INTO order_items (
							order_id,
							product_id,
							price,
							qty
						) 
						VALUES
						(%s , %s ,%s ,%s )",
						$order_id,
						$productId,
						$price,
						$qty
					);
					//echo $sql; exit();
					$query = $this->_connect->query($sql);
					//echo $sql . "<br>" ; 
					/*if($query){
						return true;
					}*/
				}
				return true;
			
			//exit();
		}
	}

?>