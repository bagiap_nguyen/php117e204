<?php  
	require MODEL_PATH . 'Product.php';

	require MODEL_PATH . 'Post.php';


	class SearchController 
	{
		protected $productModel;

		protected $postModel;

		public function __construct()
		{
			$this->productModel = new Product();

			$this->postModel = new Post;
		}

		public function index(){
			// ____________Tin tức mới
			$where = "";

			$field = "*";

			$join = "";

			$orderby = " ORDER BY posts.id DESC LIMIT 3";

			$posts = $this->postModel->getPosts($field, $join, $where, $orderby);

			$totalQTY = 0;
			if (isset($_SESSION['cart'])) {
				foreach ($_SESSION['cart'] as $item) {
					$totalQTY += $item['qty'];
				}
			}


			if (isset($_POST['submit'])) {
				$str = $_POST['str'];

				$field 		= "products.id,products.name,products.slug,products.sku,products.price,product_images.img";

				$join 		= " INNER JOIN product_images ON products.id = product_images.product_id ";

				$where 		= " products.is_featured = 1 AND products.qty > 0 AND products.status = 1";

				$orderBy	= "ORDER BY products.name DESC";

				$limit 		= " LIMIT 4 ";

				$whereNew   = $where . " AND products.name LIKE '%" . 
										$str . "%'";

				$productsNew = $this->productModel->getProducts($field,$join,$whereNew,$orderBy,$limit,'');

				$data = [
					'productsNew' => $productsNew,
					'posts' => $posts,
					'totalQTY' => $totalQTY
				];

				

				return view('search.index',$data);

			}
		}

	}

?>