<?php  
	require MODEL_PATH . 'Product.php';

	require MODEL_PATH . 'Post.php';

	require MODEL_PATH . 'Customer.php';

	require MODEL_PATH . 'Order.php';

	/**
	 * 
	 */
	class CartController
	{
		protected $Product;

		protected $postModel;

		protected $Customer;

		protected $orderModel;

		public function __construct()
		{
			$this->Product = new Product();

			$this->postModel = new Post;

			$this->Customer = new Customer;

			$this->orderModel = new Order;
		}

		public function index()
		{
			if (count($_SESSION['cart']) == 0) {
				$message = 'Chưa có sản phẩm trong giỏ hàng. Vui lòng mua hàng.';
			}
			$errors = [];
			// ______________Hiện tin tức mới____________________
			$where = "";

			$field = "*";

			$join = "";

			$orderby = " ORDER BY posts.id DESC LIMIT 3";

			$posts = $this->postModel->getPosts($field, $join, $where, $orderby);

			$provinces = $this->Customer->getProvinces();

			$data = [];
			if (isset($_POST['submit'])) {	
				
				$errors = [];

				if(!isset($_POST['phone']) || empty($_POST['phone'])){
					$errors[] = "Vui lòng điền số điện thoại";
				}

				if(!isset($_POST['address']) || empty($_POST['address'])){
					$errors[] = "Vui lòng điền Địa chỉ";
				}

				if (!isset($_POST['province_id']) || $_POST['province_id'] == "") {
					$errors[] = "Bạn chưa chọn Tỉnh/ Thành phố";
				}


				if (!isset($_POST['district_id']) || $_POST['district_id'] == "") {
					$errors[] = "Bạn chưa chọn Quận/Huyện";
				}
				if ($errors == 0) {

					$fullname = (isset($_POST['hoten'])) ? $_POST['hoten'] : $_SESSION['customer']['fullname'];

					$email = (isset($_POST['email'])) ? $_POST['email'] : $_SESSION['customer']['email'];

					$phone = trim($_POST['phone']);

					$address = trim($_POST['address']);

					$province_id = trim($_POST['province_id']);

					$district_id = trim($_POST['district_id']);

					$amount = (float)($_POST['amount']);

					$note = (isset($_POST['note'])) ? : "Chưa có lưu ý";

					if (isset($_SESSION['cart']) && isset($_SESSION['customer'])) {

						$userId = $_POST['userId'];

						$isFlag = $this->orderModel->addOrder($userId,$fullname,$email,$amount,$phone,$address, $province_id, $district_id, $note,$_SESSION['cart']);
					}	

					if (!isset($_SESSION['customer'])) {
				
						$isFlag = $this->orderModel->addOrderNew($fullname,$email,$amount,$phone,$address, $province_id, $district_id,$note,$_SESSION['cart']);
					}
				}
			}

			$totalQTY = 0;
			if (isset($_SESSION['cart'])) {
				foreach ($_SESSION['cart'] as $item) {
					$totalQTY += $item['qty'];
				}
			}

			//var_dump($_SESSION['cart']); exit();

			$data = [
				'cart' 		=> (isset($_SESSION['cart'])) ? $_SESSION['cart'] : '',
				'message' => (isset($message)) ? $message : '',
				'posts' => $posts,
				'provinces' => $provinces,
				'errors' => $errors,
				'totalQTY' => $totalQTY
			];

			return view('cart.index',$data);
		}

		public function addCart()
		{
			$id 	= $_GET['id'];

			$field 	= " products.id,products.price,products.name,products.is_sale,product_images.img ,products.is_promo "; 

			$join 	= " INNER JOIN product_images ON products.id = product_images.product_id ";

			$where = "id = '{$id}' ";

			$product = $this->Product->getProduct($field,$join,$where);


			if (!isset($_SESSION['cart'][$id]) || $_SESSION['cart'][$id] == null) {
				
				$_SESSION['cart'][$id] = [
					'id'   	=> $id,
					'name' 	=> $product['name'],
					'img'  	=> $product['img'],
					'price' => $product['price'],
					'qty'   => 1
				];

			} else {

				if (array_key_exists($id, $_SESSION['cart'])) {
				
					$_SESSION['cart'][$id]['qty'] += 1;
				}
				
			}

			$totalQTY = 0;
			$_itemHTML = '';
			$subtotal = 0;
			if (isset($_SESSION['cart'])) {
				foreach ($_SESSION['cart'] as $item) {
					$totalQTY += $item['qty'];
					$_itemHTML .= '<a class="dropdown-item" href="#">' . $item['name'] . '</a>';
					$subtotal += $item['qty'] * $item['price'];
				}
			}

			$data = [
				'items' => isset($_SESSION['cart']) ? $_SESSION['cart'] : null,
				'totalQTY' => $totalQTY,
				'itemHTML' => $_itemHTML,
				'subtotal' => 'Tổng: ' . number_format($subtotal) . ' VNĐ'
			];
			
			echo json_encode($data);exit;
		}

		public function update()
		{
			
			foreach ($_POST['qty'] as $key => $value) {
				$_SESSION['cart'][$key]['qty'] = $value;
			}

			header('Location:?c=cart&m=index');
		}

		public function delCart()
		{
			
			$id = $_GET['id'];

			unset($_SESSION['cart'][$id]);
			//echo json_encode($_SESSION['cart']);
			header('Location:?c=cart&m=index');

		}
	}

?>