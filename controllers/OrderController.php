<?php  
	require MODEL_PATH . 'Order.php';
	require MODEL_PATH . 'Post.php';
	/**
	 * Order
	 */
	class OrderController
	{
		protected $orderModel;

		protected $postModel;

		public function __construct()
		{
			$this->orderModel = new Order;

			$this->postModel = new Post;
		}

		public function index(){

			$data = [];
			// ______________Hiện tin tức mới____________________
			$where = "";

			$field = "*";

			$join = "";

			$orderby = " ORDER BY posts.id DESC LIMIT 3";

			$posts = $this->postModel->getPosts($field, $join, $where, $orderby);
			$data = [
				'posts' => $posts
			];
			return view('order.index',$data);
		}

		public function addOrder(){
			if (isset($_SESSION['cart']) && isset($_SESSION['customer'])) {
				
				
				$errors = [];

				if(!isset($_POST['phone']) || empty($_POST['phone'])){
					$errors[] = "Vui lòng điền số điện thoại";
				}

				if(!isset($_POST['address']) || empty($_POST['address'])){
					$errors[] = "Vui lòng điền Địa chỉ";
				}

				if (count($errors) == 0) {

					$userId = $_POST['userId'];

					$fullname = (isset($_POST['hoten'])) ? $_POST['hoten'] : $_SESSION['customer']['fullname'];

					$email = (isset($_POST['email'])) ? $_POST['email'] : $_SESSION['customer']['email'];

					$phone = trim($_POST['phone']);

					$address = trim($_POST['address']);

					$province_id = trim($_POST['province_id']);

					$district_id = trim($_POST['district_id']);

					$amount = (float)($_POST['amount']);

					$note = (isset($_POST['note'])) ? : "Chưa có lưu ý";

					$isFlag = $this->orderModel->addOrder($userId,$fullname,$email,$amount,$phone,$address, $province_id, $district_id, $note,$_SESSION['cart']);

					$order_id = $isFlag;

					if ($isFlag != null) {
						$order_items = $this->orderModel->addOrderItems($order_id, $_SESSION['cart']);
						echo $order_items;
						if ($order_items) {
							header("Location:?c=home&m=index");
						}
					}
				}

				
			}

			if (!isset($_SESSION['customer'])) {
				
				$fullname = (isset($_POST['hoten'])) ? $_POST['hoten'] : $_SESSION['customer']['fullname'];

				$email = (isset($_POST['email'])) ? $_POST['email'] : $_SESSION['customer']['email'];

				$phone = trim($_POST['phone']);

				$address = trim($_POST['address']);

				$province_id = trim($_POST['province_id']);

				$district_id = trim($_POST['district_id']);

				$amount = (float)($_POST['amount']);

				$note = (isset($_POST['note'])) ? : "Chưa có lưu ý";

				$isFlag = $this->orderModel->addOrderNew($fullname,$email,$amount,$phone,$address, $province_id, $district_id,$note,$_SESSION['cart']);
				$order_id = $isFlag;

				if ($isFlag != null) {
					$order_items = $this->orderModel->addOrderItems($order_id, $_SESSION['cart']);
					echo $order_items;
					if ($order_items) {
						header("Location:?c=home&m=index");
					}
				}

				/*if ($isFlag) {
						echo $isFlag;
						header("Location:?c=home&m=index");
					}*/
			}
		}
	}
?>