<?php  
	/**
	 * 
	 */
	require MODEL_PATH . 'Post.php';

	require MODEL_PATH . 'Product.php';

	class PostController 
	{	

		protected $postModel;

		protected $productModel;

		public function __construct()
		{
			$this->postModel = new Post;

			$this->productModel = new Product;
		}

		public function index()
		{
			$data = [];
			/*$where = "";

			$field = "*";

			$join = "";

			$orderby = " ORDER BY posts.id DESC";

			$posts = $this->postModel->getPosts($field, $join, $where, $orderby);
			
			$data = [
				'posts' => $posts
			];*/

			return view('post.index', $data);
		}

		public function list()
		{
			$data = [];

			//_____________Tin tức mới_____________
			$where = "";

			$field = "*";

			$join = "";

			$orderby = " ORDER BY posts.id DESC LIMIT 3";

			$posts = $this->postModel->getPosts($field, $join, $where, $orderby);
			// ______________lấy các tin tức____________________

			$totalRecord = $this->postModel->totalRecordPost();

			$field = "posts.*";

			$join = "";


			$where = "" ;

			$orderBy = '';

			$limit = " LIMIT 3 ";

			
			// Xử lý phân trang :
			// Định nghĩa 1 trang có 6 sp hiện ra
			$totalPages = ceil($totalRecord['totalRecord'] / 3 ); // Tổng số pages

			$currentPage = 1;

			if (isset($_GET['pages']) && $_GET['pages'] != '') {
				$currentPage = $_GET['pages'];
			}

			$offsetLocation = ($currentPage - 1 ) * 3;

			$offset = " OFFSET $offsetLocation";

			// Sp hiển thị ra :
			$listPost = $this->postModel->getListPost($field,$join,$where,$orderBy,$limit,$offset);

			/*---------lấy sản phẩm phụ kiện-------------*/
			$field = "products.*,
					  product_images.img as img";

			$join = "LEFT JOIN product_images ON product_images.product_id = products.id
					 LEFT JOIN product_categories ON product_categories.id = products.product_category_id";

			$where = "product_categories.parent_id = 2";
			$limit = 6;
			$offset = 0;
			$orderBy = "products.id DESC";
			$extras = $this->productModel->getProductExtras($field, $join, $where, $limit, $offset, $orderBy);

			$totalQTY = 0;
			if (isset($_SESSION['cart'])) {
				foreach ($_SESSION['cart'] as $item) {
					$totalQTY += $item['qty'];
				}
			}
			
			$data = [
				'posts' => $posts,
				'totalRecord' => $totalRecord,
				'totalPages' => $totalPages,
				'listPost' => $listPost,
				'currentPage' => $currentPage,
				'extras' => $extras,
				'totalQTY' => $totalQTY
			];

			return view('post.list', $data);
		}

		public function view()
		{
			$data = [];
			//_____________Tin tức mới_____________
			$where = "";

			$field = "*";

			$join = "";

			$orderby = " ORDER BY posts.id DESC LIMIT 3";

			$posts = $this->postModel->getPosts($field, $join, $where, $orderby);
			// ___________Lấy tin tức từ id____________

			$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
			if ($id == 0) {
				redirect('?c=post&m=list');
			}

			$field = "*";
			$join = "";
			$where = 'id = ' . $id;

			$post = $this->postModel->getPost($field, $join, $where);

			/*---------lấy sản phẩm phụ kiện-------------*/
			$field = "products.*,
					  product_images.img as img";

			$join = "LEFT JOIN product_images ON product_images.product_id = products.id
					 LEFT JOIN product_categories ON product_categories.id = products.product_category_id";

			$where = "product_categories.parent_id = 2";
			$limit = 6;
			$offset = 0;
			$orderBy = "products.id DESC";
			$extras = $this->productModel->getProductExtras($field, $join, $where, $limit, $offset, $orderBy);
			
			$data = [
				'posts' => $posts,
				'post' => $post,
				'extras' => $extras
			];
			return view('post.view', $data);
		}
	}
?>