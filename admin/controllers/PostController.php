<?php  

require MODEL_PATH . 'Post.php';

class PostController {

	protected $postModel;

	public function __construct()
	{
		$this->postModel = new Post();
	}


	public function index()
	{
		$data = [];
		$where = "";

		$field = "*";

		$join = "";

		$orderby = " ORDER BY posts.id DESC";

		$posts = $this->postModel->getPosts($field, $join, $where, $orderby);
		
		$data = [
			'posts' => $posts
		];
		return view('posts.index', $data);
	}

	public function view()
	{
		$data = [];

		$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
		if ($id == 0) {
			redirect('index.php?c=post');
		}

		$field = "posts.*,
				  post_categories.name as category_name";

		$join = "LEFT JOIN post_categories ON posts.post_category_id = post_categories.id ";

		$where = "posts.id=" . $id;

		$post = $this->postModel->getPost($field, $join, $where);

		$data = [
			'post' => $post
		];

		return view('posts.view', $data);
	}

	public function update()
	{
		$data = $errors = [];

		$post_categories = $this->postModel->getCategories();

		$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;
		if ($id == 0) {
			redirect('index.php?c=post');
		}
		$field = "*";
		$join = "";
		$where = "posts.id=" . $id;
		$post = $this->postModel->getPost($field, $join, $where);

		if (isset($_POST['reset'])) {
			redirect('index.php?c=post&m=index');
		}

		if (isset ($_POST['submit'])){
			if (!isset ($_POST['name']) || empty ($_POST['name'])){
				$errors[] = 'Bạn chưa nhập Tiêu đề';
			}
			
			if (!isset($_POST['post_category_id']) || $_POST['post_category_id'] == '') {
				$errors[] = 'Thư mục tin tức đang để trống';
			}

			$allowedExtention = ['png', 'gif', 'jpg'];
			$targetDir = "public/img/posts/";

			$targetFile = $targetDir . $_FILES["file"]["name"];
			$imageFileType = @end(explode('.', $_FILES["file"]["name"]));

			if (count ($errors) == 0){
				$id = trim ($_POST['id']);
				$name = trim ($_POST['name']);

				$str = $name;
				$slug = slugify($str);

				
				$post_category_id = trim ($_POST['post_category_id']);
				$description = trim ($_POST['description']);
				$content = trim ($_POST['content']);

				$meta_title = trim ($_POST['meta_title']);
				$meta_keyword = trim ($_POST['meta_keyword']);
				$meta_description = trim ($_POST['meta_description']);
				
				$is_featured = (isset($_POST['is_featured'])) ? $_POST['is_featured'] : 0;
				// $updated_at = trim ($_POST['updated_at']);
				$status = (isset($_POST['status'])) ? $_POST['status'] : 0;

				if (!in_array($imageFileType, $allowedExtention)) {
					$img = $post['img'];
				} else {
					$img = trim ($targetFile);

					if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFile)) {
					} else {
						$errors[] = "Sorry, there was an error uploading your file.";
					}
				}

				$posts = $this->postModel->editPost($id, $name,$slug, $post_category_id, $description, $content, $meta_title, $meta_keyword, $meta_description , $is_featured, $status);

				if ($posts = true) {
					redirect('index.php?c=post&m=index');
				}

			}

		}
		$data = [
			'errors' => $errors,
			'post_categories' => $post_categories,
			'post' => $post
		];

		return view('posts.update', $data);
	}

	public function create()
	{
		$data = $errors = [];

		$post_categories = $this->postModel->getCategories();

		if (isset($_POST['reset'])) {
			redirect('index.php?c=post&m=index');
		}

		if (isset ($_POST['submit'])){
			if (!isset ($_POST['name']) || empty ($_POST['name'])){
				$errors[] = 'Bạn chưa nhập Tiêu đề';
			}
			
			if (!isset($_POST['post_category_id']) || $_POST['post_category_id'] == '') {
				$errors[] = 'Thư mục tin tức đang để trống';
			}

			$allowedExtention = ['png', 'gif', 'jpg'];
			$targetDir = "public/img/posts/";

			$targetFile = $targetDir . $_FILES["file"]["name"];
		    $imageFileType = @end(explode('.', $_FILES["file"]["name"]));

		    if (!in_array($imageFileType, $allowedExtention)) {
		        $img = '';
		    } else {
		    	$img = trim ($targetFile);
		    }

			if (count ($errors) == 0){
				
				$name = trim ($_POST['name']);

				$str = $name;
				$slug = slugify($str);

				
				$post_category_id = trim ($_POST['post_category_id']);
				$description = trim ($_POST['description']);
				$content = trim ($_POST['content']);

				$meta_title = trim ($_POST['meta_title']);
				$meta_keyword = trim ($_POST['meta_keyword']);
				$meta_description = trim ($_POST['meta_description']);
				
				$is_featured = (isset($_POST['is_featured'])) ? $_POST['is_featured'] : 0;
				// $updated_at = trim ($_POST['updated_at']);
				$status = (isset($_POST['status'])) ? $_POST['status'] : 0;

				if (move_uploaded_file($_FILES["file"]["tmp_name"], $targetFile)) {
		        } else {
		            $errors[] = "Sorry, there was an error uploading your file.";
		        }

				$posts = $this->postModel->addPost($name, $slug, $post_category_id, $description, $content, $meta_title, $meta_keyword, $meta_description , $is_featured, $status);

				if ($posts = true) {
					redirect('index.php?c=post&m=index');
				}

			}
		}

		$data = [
			'errors' => $errors,
			'post_categories' => $post_categories
		];
		return view('posts.create', $data);
	}

	public function delete()
	{
		$id = isset($_GET['id']) ? (int) $_GET['id'] : 0;

		if ($id == 0) {
			redirect('index.php?post');
		}

		$field = "posts.*,
				  post_categories.name as category_name";

		$join = "LEFT JOIN post_categories ON posts.post_category_id = post_categories.id ";

		$where = "posts.id=" . $id;

		$post = $this->postModel->getPost($field, $join, $where);
		
		if (!is_null($post)) {
			$this->postModel->deletePost($id);
		}

		redirect('index.php?c=post');
	}
}

?>