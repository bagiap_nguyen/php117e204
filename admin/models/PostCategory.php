<?php 

class PostCategory extends Database {

	public function getCategories($where = '')
	{	
		$condition = '';
		if ($where != '') {
			$condition = 'AND ' . $where;
		}

		$sql = sprintf("SELECT * FROM post_categories WHERE 1=1 %s ", $condition);

	
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_all(MYSQLI_ASSOC);
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;

	}

	public function addCategory($name, $slug, $img, $description, $parent_id, $meta_title, $meta_keyword, $meta_description, $status)
	{
		$sql = "INSERT INTO post_categories (name, slug, img, description, parent_id, meta_title, meta_keyword, meta_description, status) VALUES ('$name', '$slug', '$img', '$description', '$parent_id', '$meta_title', '$meta_keyword', '$meta_description', '$status')";
		try {
			$query = $this->_connect->query($sql);

			if ($query) {
				return true;
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
 		}
		return false;
	}
	
	public function editCategory($id, $name, $slug, $img, $description, $parent_id, $meta_title, $meta_keyword, $meta_description, $status)
	{
		$sql = "UPDATE post_categories SET name = '$name', slug = '$slug', img = '$img', description = '$description', parent_id = '$parent_id', meta_title = '$meta_title', meta_keyword = '$meta_keyword', meta_description = '$meta_description', status = '$status' WHERE id = $id";
		try {

			$query = $this->_connect->query($sql);
			if ($query) {
				return true;
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return false;
	}

	public function getCategory($where ='')
	{
		$condition = '';
		if ($where != '') {
			$condition = 'AND ' . $where;
		}

		$sql = sprintf("SELECT * FROM post_categories WHERE 1=1 %s LIMIT 1", $condition);
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_assoc();
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

	public function deleteCategory($id)
	{
		
		$sql = "DELETE FROM comments WHERE post_id  IN (SELECT id FROM posts WHERE post_category_id   = '{$id}')";
		
		try {

			$query = $this->_connect->query($sql);
			
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		// ______________________ Delete posts __________________
		$sql = "DELETE FROM posts WHERE post_category_id  = '{$id}'";
		try {

			$query = $this->_connect->query($sql);
			
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		// ______________________ Delete post_categories __________________
		$sql = "DELETE FROM post_categories WHERE id='{$id}'";
		try {

			$query = $this->_connect->query($sql);
			if ($query) {
				return true;
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return false;
	}
}
?>