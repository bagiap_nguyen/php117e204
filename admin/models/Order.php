<?php 
/**
* 
*/
class Order extends Database
{

	public function getOrders($where = '', $filed = '*', $join = '', $orderBy = null,$limit = null)
		{
			$condition = '';

			if ($where != '') {
				$condition = ' AND ' . $where;
			}

			$sql = sprintf("SELECT %s FROM orders %s WHERE 1=1 %s %s 
							%s ",$filed, $join, $condition,$orderBy,$limit);
			//echo $sql;exit();
			
			$query = $this->_connect->query($sql);

			if ($query) {
				return $query->fetch_all(MYSQLI_ASSOC);
			}

			return null;

		}	

		public function getOrder($field,$where)
		{
			$sql 	= sprintf("SELECT %s FROM orders WHERE %s ",$field,$where);

			$query	= $this->_connect->query($sql);

			if ($query) {
			 	return $query->fetch_assoc();
			}

			return null; 
		}
		public function createOrder($user_id,$fullname,$email,$phone,$address,$provicence_id,$district_id,$amount,$note,$created_at,$updated_at,$status)
		{
			$created_at = date("Y-m-d");
			$sql = sprintf("INSERT INTO orders(id, user_id,fullname, email, phone, address, provicence_id, district_id, amount, note, created_at, updated_at,status)
				VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ", "'{$user_id}'"
				,"'{$fullname}'","'{$email}'","'{$phone}'","'{$address}'"
				,"'{$provicence_id}'","'{$district_id}'","'{$amount}'","'{$note}'","'{$created_at}'","'{$updated_at}'","'{$status}'");

			$query = $this->_connect->query($sql);

			if ($query) {
				return true;
			}

			return false;
		}
		public function updateOrder($user_id,$fullname,$email,$phone,$address,$provicence_id,$district_id,$amount,$note,$created_at,$updated_at,$status,$where = null)
		{

			$updated_at = date("Y-m-d");

			$set = " 
				user_id 		= '{$user_id}',

				fullname 		= '{$fullname}',

				email   	 	= '{$email}',

				phone 		 	= '{$phone}',

				address  		= '{$address}',

				provicence_id    		= '{$provicence_id}',

				district_id 		= '{$district_id}',

				amount  		= '{$amount}',

				note  		= '{$note}',

				created_at  		= '{$created_at}',

				updated_at  		= '{$updated_at}',

				status 			= '{$status}'
			
			";

			$sql 		= sprintf("UPDATE orders SET %s 
							WHERE %s",$set,$where);

			$edit 		= $this->_connect->query($sql);

			if ($edit) {
				return true;
			} else {
				return false;
			}

		}
		public function deleteOrder($id)
		{

			$sql = "DELETE FROM order_items WHERE order_id  = '{$id}'";
			try {

				$query = $this->_connect->query($sql);
				
			} catch (Exception $ex) {
				die($ex->getMessage());
			}

			// ______________________ Delete product_categories __________________
			$sql = "DELETE FROM orders WHERE id='{$id}'";
			try {

				$query = $this->_connect->query($sql);
				if ($query) {
					return true;
				}
			} catch (Exception $ex) {
				die($ex->getMessage());
			}

			return false;
		}

		public function getItems($field = '*', $join, $where)
		{
			$condition = '';

			if ($where != '') {
				$condition = ' AND ' . $where;
			}

			$sql = sprintf("SELECT %s FROM order_items %s WHERE 1=1 %s ",$field, $join, $condition);
			//echo $sql; exit();
			
			try {
				$query = $this->_connect->query($sql);
				if ($query) {
					return $query->fetch_all(MYSQLI_ASSOC);
				}
			} catch (Exception $ex) {
				die($ex->getMessage());
			}
		}

		public function getPrices($field = '*', $join, $where)
		{
			$condition = '';

			if ($where != '') {
				$condition = ' AND ' . $where;
			}

			$sql = sprintf("SELECT %s FROM order_items %s WHERE 1=1 %s ",$field, $join, $condition);
			// echo $sql; exit();
			
			try {
				$query = $this->_connect->query($sql);
				if ($query) {
					return $query->fetch_assoc();
				}
			} catch (Exception $ex) {
				die($ex->getMessage());
			}
		}
		
}
 ?>