<?php  

class Post extends Database {

	public function getPosts($field = '*',$join = '', $where = '', $orderby = '')
	{
		$condition = '';
		if ($where != '') {
			$condition = 'AND ' . $where;
		}

		$sql = sprintf("SELECT %s FROM posts %s WHERE 1=1 %s %s", $field, $join, $condition, $orderby);
		//echo $sql; exit();
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_all(MYSQLI_ASSOC);
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

	public function getPost($field, $join, $where)
	{
		$condition = '';
		if ($where != '') {
			$condition = 'AND ' . $where;
		}

		$sql = sprintf("SELECT %s FROM posts %s WHERE 1=1 %s", $field, $join, $condition);
		//echo $sql; exit();
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_assoc();
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

	public function getCategories()
	{
		$sql = sprintf("SELECT * FROM post_categories");
		//echo $sql; exit();
		try {
			$query = $this->_connect->query($sql);
			if ($query) {
				return $query->fetch_all(MYSQLI_ASSOC);
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}

	public function editPost($id, $name,$slug, $post_category_id, $description, $content, $meta_title, $meta_keyword, $meta_description , $is_featured, $status)

	{
		$updated_at = date('Y-m-d');
		$sql = "UPDATE posts SET name = '$name', slug = '$slug', post_category_id = '$post_category_id', description = '$description', content = '$content', meta_title = '$meta_title', meta_keyword = '$meta_keyword', meta_description = '$meta_description', is_featured = '$is_featured', updated_at = '$updated_at' WHERE id = '$id'";
		try {
			$query = $this->_connect->query($sql);

			if ($query) {
				return $this->_connect->insert_id;
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
 		}
		return false;
	}

	public function addPost($name, $slug, $post_category_id, $description, $content, $meta_title, $meta_keyword, $meta_description , $is_featured, $status)
	{
		$created_at = date('Y-m-d');

		$sql = "INSERT INTO posts (name, slug, post_category_id, description, content, meta_title, meta_keyword, meta_description, is_featured, created_at, status) VALUES ('$name', '$slug', '$post_category_id', '$description', '$content', '$meta_title', '$meta_keyword', '$is_featured','$created_at', '$status')";
		
		try {
			$query = $this->_connect->query($sql);

			if ($query) {
				return $this->_connect->insert_id;
				//exit();
			}
		} catch (Exception $ex) {
			die($ex->getMessage());
 		}
		return false;
	}

	public function deletePost($id)
	{
		$sql = "DELETE FROM comments WHERE post_id = '{$id}'";
		
		try {

			$query = $this->_connect->query($sql);
			
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		// ______________________ Delete posts __________________
		$sql = "DELETE FROM posts WHERE id = '{$id}'";
		try {

			$query = $this->_connect->query($sql);
			
		} catch (Exception $ex) {
			die($ex->getMessage());
		}

		return false;
	}
}

?>