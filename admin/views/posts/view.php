<script src="public/tinymce/tinymce.min.js"></script>
 <script>
  tinymce.init({
  selector: 'textarea',
  height: 200,
  theme: 'modern',
  plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
  toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
  image_advtab: true,
  templates: [
    { title: 'Test template 1', content: 'Test 1' },
    { title: 'Test template 2', content: 'Test 2' }
  ],
  content_css: [

  ]
 });
</script>

<div class="right_col" role="main">
<div class="">
<div class="page-title">
  <div class="title_left">
    <h3>Thông tin Tin tức</h3>
  </div>
  <div class="title_right" style="float: right;">
      <div class="add-post" style="font-size: 14px;border-radius: 10px;padding: 10px;float: right;">
      <!-- <a href="index.php?c=postReview&m=index&id=<?php echo $post['id'];?>" style="color: #BA5905;">Review tin tức</a> -->
  </div>
</div>
<div class="clearfix"></div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo $post['name'] ;?></h2>
        <div class="add-post" style="float: right;">
      <a href="index.php?c=post&m=update&id=<?php echo $post['id'];?>" style="color: #BA5905;">Sửa tin tức</a>
      </div>
        <div class="clearfix"></div>

      <div class="x_content">

        <form class="form-horizontal form-label-left" novalidate action="" method="post" enctype="multipart/form-data">
          <span class="section"> </span>

          <div class="item form-group">

            <label class="control-label col-md-3 col-sm-3 col-xs-12">ID
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="id" class="form-control col-md-7 col-xs-12" readonly="readonly" data-validate-length-range="6" data-validate-words="2" name="id" readonly="readonly" type="text" value="<?php echo $post['id'] ;?>">
            </div>
          </div>

          <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Tiêu dề
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="name" readonly="readonly" type="text" value="<?php echo $post['name'] ;?>">
              </div>
          </div>

          <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12">Hình ảnh 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <p>
                  <img src="<?php echo $post['img'];?>" style="width: 100%;">
                </p>
              </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Thư mục
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input id="category_name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" data-validate-words="2" name="category_name"  readonly="readonly" type="text" value="<?php echo $post['category_name'];?>">
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Miêu tả
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea  rows="6" type="text" readonly="readonly" name="description" class="form-control col-md-7 col-xs-12" value=""><?php echo $post['description'] ;?></textarea>
            </div>
          </div>

          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nội dung
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea  rows="6" type="text" readonly="readonly" name="content" class="form-control col-md-7 col-xs-12" value=""><?php echo $post['content'] ;?></textarea>
            </div>
          </div>

 
          <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Thời gian tạo
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="date" name="created_at" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo $post['created_at'] ;?>">
            </div>
          </div>

           <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12">Status
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="index" name="status" readonly="readonly" class="form-control col-md-7 col-xs-12" value="<?php echo displayStatus($post['status']);?>">
            </div>
          </div>

          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
              <button type="submit" name="reset" class="btn btn-primary">Cancel</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

