<!-- <?php 
  var_dump($data);

 ?>  -->
<!-- page content -->
<style type="text/css">

</style>
        <div class="right_col" role="main">
          <!-- top tiles -->
          <div class="container">
            <div class="row">
              
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>10 đơn hàng mới nhất<small><a href="?c=order&m=index">Xem tất cả</a></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                        
                      </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Mã đơn hàng</th>
                          <th>Tên khách hàng</th>
                          <th>Phiếu giá</th>
                          <th>Trạng thái</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if (count($orderNews) > 0) : 
                          foreach ($orderNews as $item) :
                        ?>
                             
                        <tr>
                          <th scope="row"><?php echo $item['id'];?></th>
                          <td><?php echo $item['fullname'];?></td>
                          <td><?php echo number_format($item['price']) . " VND" ;?></td>
                          <td><?php echo displayStatus($item['status']);;?></td>
                        </tr>
                        <?php
                            endforeach; 
                          endif;
                        ?>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
              
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>10 khách hàng mới nhất<small><a href="?c=customer&m=index">Xem tất cả</a></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                        
                      </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <table class="table">
                      <thead>
                        <tr>
                          <th>Tên khách hàng</th>
                           <th>Email</th>
                           <th>Số điện thoại</th>
                           <th>Địa chỉ</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if (count($customerNews) > 0) : 
                          foreach ($customerNews as $item) :
                        ?>
                             
                        <tr>
                          <th scope="row"><?php echo $item['fullname'];?></th>
                          <td><?php echo $item['email'];?></td>
                          <td><?php echo $item['phone'];?></td>
                          <td><?php echo $item['address'];?></td>
                        </tr>
                        <?php
                            endforeach; 
                          endif;
                        ?>
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
              <!-- <div class="col-sm-4">
               <div class="row">
                 <h5>10 sản phẩm mới nhất</h5>
               </div>
               <div class="row">
                 <table border="1">
                   <thead>
                     <th>Mã sản phẩm</th>
                     <th>Tên sản phẩm</th>
                     <th>Hình ảnh sản phẩm</th>
                     <th>Giá sản phẩm</th>
                   </thead>
                   <tbody>
                     <?php
                        if (count($products) > 0) : 
                            $i = 0;
                            foreach ($products as $item) :
                                $i++;
                                ?>
                                <tr>
                                    <td><?php echo $item['id'];?></td>
                                    <td><?php echo $item['name'];?></td>
                                    <td><img style="width: 50px;" src="<?php echo $item['img'] ;?>"></td>
                                    <td><?php echo number_format($item['price']) . " VND" ;?></td>
                                </tr>
                                <?php
                            endforeach;
                        else: 
                            ?>
                            <tr><td colspan="8">Chưa có bản ghi</td></tr>
                            <?php
                        endif; 
                        ?>
                   </tbody>
                 </table>
               </div>
               <div class="row">
                 <a href="?c=product&m=index">Xem tất cả</a>
               </div>
              </div> -->
            </div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>10 sản phẩm mới nhất<small><a href="?c=product&m=index">Xem tất cả</a></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"></a>
                        
                      </li>
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>

                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title">Số thứ tự </th>
                            <th class="column-title">Mã sản phẩm</th>
                            <th class="column-title">Tên sản phẩm</th>
                            <th class="column-title">Hình ảnh sản phẩm</th>
                            <th class="column-title">Giá sản phẩm</th>
                            <th class="column-title">Chi tiết</th>
                          </tr>
                        </thead>

                        <tbody>
                          <?php
                            if (count($products) > 0) : 
                             $i = 0;
                              foreach ($products as $item) :
                                $i++;
                          ?>
                          
                          <tr class="even pointer">
                            <td class=" "><?php echo $i; ?></td>
                            <td class=" "><?php echo $item['id'];?></td>
                            <td class=" "><?php echo $item['name'];?></i></td>
                            <td class=" "><img style="width: 50px;" src="<?php echo $item['img'] ;?>"></td>
                            <td class=" "><?php echo number_format($item['price']) . " VND" ;?></td>
                            <td class="a-right a-right "><a href="index.php?c=product&m=view&id=<?php echo $item['id'];?>" title="">Thông tin</a></td>
                            
                            </td>
                          </tr>

                          <?php  
                            endforeach;
                          endif;
                          ?>
                          
                        </tbody>
                      </table>
                    </div>
              
            
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content